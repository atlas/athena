/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/MeasurementToTrackParticleDecoration.h"
#include "src/PixelClusterTruthDecorator.h"
#include "src/StripClusterTruthDecorator.h"
#include "src/PixelClusterSiHitDecoratorAlg.h"

DECLARE_COMPONENT(ActsTrk::MeasurementToTrackParticleDecoration)
DECLARE_COMPONENT(ActsTrk::PixelClusterTruthDecorator)
DECLARE_COMPONENT(ActsTrk::StripClusterTruthDecorator)
DECLARE_COMPONENT(ActsTrk::PixelClusterSiHitDecoratorAlg)
