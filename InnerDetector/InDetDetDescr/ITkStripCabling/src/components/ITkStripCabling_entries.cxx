/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "../ITkStripCablingAlg.h"
#include "../ITkStripCablingTool.h"

DECLARE_COMPONENT(ITkStripCablingAlg)
DECLARE_COMPONENT(ITkStripCablingTool)

