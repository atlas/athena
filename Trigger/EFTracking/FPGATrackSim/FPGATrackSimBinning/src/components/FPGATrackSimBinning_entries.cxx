#include "FPGATrackSimBinning/FPGATrackSimBinStep.h"
#include "FPGATrackSimBinning/FPGATrackSimBinTool.h"
#include "FPGATrackSimBinning/FPGATrackSimBinnedHits.h"
#include "../FPGATrackSimKeyLayerBinDesc.h"

DECLARE_COMPONENT( FPGATrackSimBinStep )
DECLARE_COMPONENT( FPGATrackSimBinTool )
DECLARE_COMPONENT( FPGATrackSimBinnedHits )
DECLARE_COMPONENT( FPGATrackSimKeyLayerBinDesc )

