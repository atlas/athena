/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "RootCollection.h"
#include "CollectionCommon.h"
#include "RootCollectionQuery.h"

#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"

#include "RootCollection/AttributeListLayout.h"

#include "PersistentDataModel/Token.h"
#include "POOLCore/Exception.h"
#include "RootUtils/APRDefaults.h"

#include "CollectionBase/ICollectionColumn.h"
#include "CollectionBase/CollectionBaseNames.h"

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IFileMgr.h"
#include "GaudiKernel/IService.h"

#include "TFile.h"
#include "TNetFile.h"
#include "TTree.h"
#include "TSocket.h"
#include "TMessage.h"
#include "TDirectory.h"

#define corENDL coral::MessageStream::endmsg

#include <map>
#include <vector>
#include <deque>
#include <ctype.h>


using namespace std;

namespace pool {

  namespace RootCollection { 

    const char* const RootCollection::c_tokenBranchName = "Token";
    const char* const RootCollection::c_attributeListLayoutName = "Schema"; 

     RootCollection::RootCollection(
        const pool::ICollectionDescription* description,
        pool::ICollection::OpenMode mode,
        pool::ISession* )
      : m_description( *description ),
      m_name( description->name() ),
      m_fileName( description->name() + ".root" ),
      m_mode( mode ),
      m_tree( 0 ),
      m_file( 0 ),
      m_session( 0 ),
      m_open( false ),
      m_readOnly( mode == ICollection::READ ? true : false ),
      m_schemaWritten( true ),
      m_poolOut( "RootCollection")
    {
       RootCollection::open();
    }

     
     RootCollection::~RootCollection()
     {
        if( m_open ) try {
           RootCollection::close();
        } catch( std::exception& exception ) {
           m_poolOut << coral::Error << exception.what() << corENDL;
           cleanup();
        }
        else cleanup();
     }


     void RootCollection::addTreeBranch( const std::string& name, const std::string& type_name )
     {
        static const std::map< std::string, char > typeDict = {
           // primitive types supported in ROOT (4.00.08) TTrees 
           //  - C : a character string terminated by the 0 character
           //  - B : an 8 bit signed integer (Char_t)
           //  - b : an 8 bit unsigned integer (UChar_t)
           //  - S : a 16 bit signed integer (Short_t)
           //  - s : a 16 bit unsigned integer (UShort_t)
           //  - I : a 32 bit signed integer (Int_t)
           //  - i : a 32 bit unsigned integer (UInt_t)
           //  - F : a 32 bit floating point (Float_t)
           //  - D : a 64 bit floating point (Double_t)
           //  - L : a 64 bit signed integer (Long64_t)
           //  - l : a 64 bit unsigned integer (ULong64_t)
           { "double", 'D' },
           { "long double", 'D' },        // only 64 bit doubles are supported 
           { "float", 'F' },
           { "int", 'I' },
           { "long", 'I' },
           { "unsigned int", 'i' },
           { "unsigned long", 'i' },
           { "long long", 'L' },
           { "unsigned long long", 'l' },
           { "short", 'S' },
           { "unsigned short", 's' },
           { "char", 'B' },
           { "unsigned char", 'b' },
           { "bool", 'B' },
           { "string", 'C' },
           { "Token", 'C' },
        };

        std::string type = "/?";
        auto it = typeDict.find (type_name);
        if (it != typeDict.end()) {
          type[1] = it->second;
        }
        std::string leaflist = name + type;
        m_tree->Branch( name.c_str(), 0, leaflist.c_str() );
     
        m_schemaWritten = false;
        m_poolOut << coral::Debug << "Created Branch " <<  name
                  << ", Type=" <<  type_name << coral::MessageStream::endmsg;
     }

     void  RootCollection::delayedFileOpen( const std::string& method )
     {
        if( m_open && !m_file && m_session && m_mode != ICollection::READ ) {
           m_file = TFile::Open(m_fileName.c_str(), pool::RootCollection::poolOptToRootOpt[m_mode] );
           if(!m_file || m_file->IsZombie()) {
              throw pool::Exception( string("ROOT cannot \"") + pool::RootCollection::poolOptToRootOpt[m_mode] 
                                     + "\" file " + m_fileName,
                                     std::string("RootCollection::") + method,
                                     "RootCollection" );
           }
           m_poolOut << coral::Info << "File " << m_fileName << " opened in " << method <<  coral::MessageStream::endmsg;
 
           m_tree->SetDirectory(m_file);
           if( !m_schemaWritten ) {
              m_tree->GetCurrentFile()->cd();
              AttributeListLayout all( m_description );
              m_poolOut << coral::Debug << "###### Writing schema...." << coral::MessageStream::endmsg;
              all.Write( RootCollection::c_attributeListLayoutName, TObject::kOverwrite );
              m_schemaWritten = true;
           }
        }
     }


     TTree*  RootCollection::getCollectionTree()
     {
        TTree *tree( NULL );
        if( m_file ) {
           tree = dynamic_cast<TTree*>(m_file->Get(APRDefaults::TTreeNames::EventTag));
           if( tree )
              m_poolOut << coral::Debug << "Retrieved Collection TTree  \""
                        << tree->GetName() << "\" from file " << m_fileName
                        << coral::MessageStream::endmsg;
        }
        return tree;
     }
        
        
     
     void RootCollection::insertRow( const pool::CollectionRowBuffer& inputRowBuffer )
     {
        if( m_mode == pool::ICollection::READ ) {
           throw pool::Exception( "Cannot modify the data of a collection in READ open mode.", "RootCollection::insertRow", "RootCollection" );
        }
        std::map< std::string, TBranch* > branchByName;
        const TObjArray* branches = m_tree->GetListOfBranches();
        Int_t nbranches = branches->GetEntriesFast();
        for(int i = 0; i < nbranches; ++i) {
           TBranch* branch = (TBranch*)branches->UncheckedAt(i);
           branchByName[ branch->GetName() ] = branch;
        }
        std::deque<std::string> stringBuffer;
        for( pool::TokenList::const_iterator iToken = inputRowBuffer.tokenList().begin();
              iToken != inputRowBuffer.tokenList().end(); ++iToken )  {
           stringBuffer.push_back( iToken->toString() );
           branchByName[ iToken.tokenName() ]->SetAddress( stringBuffer.back().data() );
        }
        coral::AttributeList attribs_nc = inputRowBuffer.attributeList();
        for( coral::Attribute& att : attribs_nc ) {
           if( att.specification().type() == typeid(std::string) ) {
              std::string&       str = att.data<std::string>();
              branchByName[ att.specification().name() ]->SetAddress( str.data() );
           } else {
              branchByName[ att.specification().name() ]->SetAddress( att.addressOfData() );
           }
        }
	if( m_tree->Fill() <= 0 ) throw pool::Exception( "TTree::Fill() failed", "RootCollection::insertRow", "RootCollection" );
     }
        
        
     
     void RootCollection::commit( bool )
     {
        delayedFileOpen("commit");
        if( m_open ) {

	  if (m_tree->GetCurrentFile() == 0) {
	    m_poolOut << coral::Debug << "setting TFile for " 
		      << m_tree->GetName() << " to " << m_file->GetName()
		      << coral::MessageStream::endmsg;
	    m_tree->SetDirectory(m_file);
	  }

           m_poolOut << coral::Debug
                     << "Commit: saving collection TTree to file: " << m_tree->GetCurrentFile()->GetName()
                     << coral::MessageStream::endmsg;

           Long64_t bytes = m_tree->AutoSave();

           m_poolOut << "   bytes written to TTree " << (size_t)bytes
                     << coral::MessageStream::endmsg;
        }
     }

     
    void RootCollection::close()
    {
       m_poolOut << coral::Info << "Closing " << (m_open? "open":"not open")
                 << " collection '" << m_fileName << "'" << coral::MessageStream::endmsg;
       if(m_open) {
          delayedFileOpen("close");
              
          if( m_mode == ICollection::CREATE || m_mode == ICollection::CREATE_AND_OVERWRITE ) {
             TObject* tree = getCollectionTree();
             if( tree )
                m_mode = ICollection::UPDATE;
             else {
                // probably supposed to unregister if the collection was not created
                m_mode = ICollection::CREATE_AND_OVERWRITE;
                if(m_fileCatalog){
                   m_fileCatalog->start();
                   m_fileCatalog->deleteFID( m_fileCatalog->lookupPFN(m_fileName) );
                   m_fileCatalog->commit();
                }
             }
          }
          if( m_mode != ICollection::READ ) {
             if( !m_schemaWritten ) {
                m_tree->GetCurrentFile()->cd();
                AttributeListLayout all( m_description );
                m_poolOut << coral::Debug << "###### Writing schema...." << coral::MessageStream::endmsg;
                all.Write( RootCollection::c_attributeListLayoutName, TObject::kOverwrite );
                m_schemaWritten = true;
             }
             // m_tree->Print();
             // m_file->Write( "0", TObject::kOverwrite );
          }
          cleanup();
       }
    }

     
    void RootCollection::cleanup()
    {
       if( m_file ) {
          int n = 0;
          if( m_fileMgr ) {
             n = m_fileMgr->close(m_file, "RootCollection");
          } else {
             m_file->Close();
          }
          if( n==0 ) delete m_file; 
          m_file = 0;
       }
       m_tree = 0;
       m_open = false;
    }       
       
     
    void RootCollection::open()  try
    {
      const string myFileType = "RootCollectionFile";

      if( m_open ) close();

      if( !m_fileCatalog
        && m_fileName.starts_with ( "PFN:")
        && m_description.connection().empty() )
      {
        // special case with no catalog and PFN specified
        // create the collection with exactly PFN file name
        m_fileName = m_description.name().substr(4);   // remove the PFN: prefix
      }
      else if( fileCatalogRequired() ) {
        m_fileName = "";

        if(!m_fileCatalog)
           m_fileCatalog = make_unique<pool::IFileCatalog>();
        
        if( m_mode == ICollection::CREATE ){
          string fid = retrieveFID();
          if(fid!="")
            throw pool::Exception( "Cannot CREATE already registered collections",
            "RootCollection::open", 
            "RootCollection");
          else{
            m_fileName = retrievePFN();
            FileCatalog::FileID dummy;
            m_fileCatalog->start();
            m_fileCatalog->registerPFN( m_fileName, myFileType, dummy);
            m_fileCatalog->commit();
          }
        }

        else if(m_mode == ICollection::CREATE_AND_OVERWRITE){
          string fid = retrieveFID();
          if(fid!="")
            m_fileName = retrieveUniquePFN(fid);
          else{
            m_fileName = retrievePFN();
            FileCatalog::FileID dummy;
            m_fileCatalog->start();
            m_fileCatalog->registerPFN( m_fileName, myFileType, dummy);
            m_fileCatalog->commit();
          }
        }

        else if(m_mode == ICollection::UPDATE){
          string fid = retrieveFID();
          if(fid!="")
            m_fileName = retrieveUniquePFN(fid);
          else
            throw pool::Exception( "Cannot UPDATE non registered collections",
            "RootCollection::open", 
            "RootCollection");
        }

        else if(m_mode == ICollection::READ) {
          string fid = retrieveFID();
          if(fid!="") {
             string dummy;
             m_fileCatalog->start();
             m_fileCatalog->getFirstPFN(fid, dummy, dummy);
             m_fileCatalog->commit();
          }else
             throw pool::Exception( "Cannot READ non registered collections",
                                    "RootCollection::open", 
                                    "RootCollection");
        }
      }

      TDirectory::TContext dirctxt;
      if( m_session == 0 || m_mode == ICollection::READ || m_mode == ICollection::UPDATE ) {
         // first step: Try to open the file
         m_poolOut << coral::Info << "Opening Collection File " << m_fileName << " in mode: "
                   << pool::RootCollection::poolOptToRootOpt[m_mode] << coral::MessageStream::endmsg;
         bool fileExists = !gSystem->AccessPathName( m_fileName.c_str() );
         m_poolOut << coral::Debug << "File " << m_fileName
		   << (fileExists? " exists." : " does not exist." ) << corENDL;
         // open the file if it exists, or create if requested
         if( !fileExists && m_mode != ICollection::CREATE && m_mode != ICollection::CREATE_AND_OVERWRITE )
            m_file = 0;
         else {
            const char* root_mode = pool::RootCollection::poolOptToRootOpt[m_mode];
            Io::IoFlags io_mode = pool::RootCollection::poolOptToFileMgrOpt[m_mode];

            if( fileExists && (m_mode == ICollection::CREATE
                               || m_mode == ICollection::CREATE_AND_OVERWRITE ) ) {
               // creating collection in an existing file
               root_mode = "UPDATE";
	       io_mode = (Io::WRITE | Io::APPEND);
            }
            if( !m_fileMgr ) {
               m_fileMgr = Gaudi::svcLocator()->service("FileMgr");
               if ( !m_fileMgr ) {
                  m_poolOut << coral::Error 
                            << "unable to get the FileMgr, will not manage TFiles"
                            << coral::MessageStream::endmsg;
               }
	    }
	    // FIXME: hack to avoid issue with setting up RecExCommon links
	    if (m_fileMgr &&
		m_fileMgr->hasHandler(Io::ROOT).isFailure()) {
	      m_poolOut << coral::Info 
			<< "Unable to locate ROOT file handler via FileMgr. "
			<< "Will use default TFile::Open" 
			<< coral::MessageStream::endmsg;
	      m_fileMgr.reset();
	    }

	    if (!m_fileMgr) {
	      m_file = TFile::Open(m_fileName.c_str(), root_mode);
	    } else {
	      void *vf(0);
	      // open in shared mode only for writing
	      bool SHARED(false);
	      if (io_mode.isWrite()) {
		SHARED = true;
	      }	       
	      int r = m_fileMgr->open(Io::ROOT,"RootCollection",m_fileName,io_mode,vf,"TAG",SHARED);
	      if (r < 0) {
		m_poolOut << coral::Error << "unable to open \"" << m_fileName
			  << "\" for " << root_mode
			  << coral::MessageStream::endmsg;
	      } else {      
		m_file = (TFile*)vf;
	      }
	    }
         }
         if(!m_file || m_file->IsZombie()) {
            throw pool::Exception( string("ROOT cannot \"") + pool::RootCollection::poolOptToRootOpt[m_mode] 
                                   + "\" file " + m_fileName,
                                   "RootCollection::open", 
                                   "RootCollection" );
         }
         m_poolOut << coral::Info << "File " << m_fileName << " opened" << coral::MessageStream::endmsg;
      }

      if( m_mode == ICollection::READ || m_mode == ICollection::UPDATE ) {
         // retrieve the TTree from file 
         m_tree = getCollectionTree();
         if( !m_tree ) {
	   // m_file->Write();
	   
	   int n(0);
	   if (!m_fileMgr) {
	     m_file->Close();
	   } else {
	     n = m_fileMgr->close(m_file,"RootCollection");
	   }

	   if (n == 0) delete m_file; 
	   m_file=0;
            throw pool::Exception( string("POOL Collection TTree not found in file ") + m_fileName,
                                   "RootCollection::open", 
                                   "RootCollection" );
         }

         AttributeListLayout* all = dynamic_cast<AttributeListLayout*>( m_tree->GetCurrentFile()->Get(RootCollection::c_attributeListLayoutName) );
         CollectionDescription desc( m_description.name(), m_description.type(), m_description.connection() );
         // clear the description
         m_description = desc;
         if( all ) {
            // Copy the specification to collection description
            all->fillDescription( m_description );
            delete all;
         } else {
            m_poolOut << coral::Warning << " Collection Description not found in file, reconstructing " <<  corENDL;
            bool      foundToken = false;
            for( int i = 0; i < m_tree->GetNbranches(); i++ ) {
               TBranch* branch = (TBranch*)m_tree->GetListOfBranches()->UncheckedAt(i);
               std::string column_name = branch->GetName();
               std::string column_type = branch->GetTitle();
               m_poolOut << coral::Debug << "  + adding column: " << column_name <<  corENDL;
               m_poolOut << coral::Debug << "      column type: " << column_type <<  corENDL;
               if( column_type.substr(0,5) != "Token" ) {
                  m_description.insertColumn( column_name, column_type.substr(0, column_type.size() -2) );
               } else {
                  if( !foundToken ) {
                     foundToken = true;
                     m_description.setEventReferenceColumnName( column_name );
                  } else {
                     throw pool::Exception( "can't reconstruct Description if more than one Token column",
                        "pool::RootCollection::readAttributeListSpecification",
                        "RootCollection" );
                  }
               }
            }
            if( !foundToken ) {
               m_description.setEventReferenceColumnName( "DummyRef" );
            }
         }
      }

      if( m_mode == ICollection::CREATE || m_mode == ICollection::CREATE_AND_OVERWRITE ) {
        // create a new TTree
        if( 0 && m_mode == ICollection::CREATE_AND_OVERWRITE ) {
          m_poolOut << coral::Warning <<  "Cleaning previous collection object from the file..." << coral::MessageStream::endmsg;
          std::string treeName = std::string(APRDefaults::TTreeNames::EventTag) + ";*";
          m_file->Delete(treeName.c_str());
          m_file->Delete("Schema;*");
        }
        m_tree = new TTree(APRDefaults::TTreeNames::EventTag, m_name.c_str());
        m_poolOut << coral::Debug << "Created Collection TTree. Collection file will be " << m_fileName << coral::MessageStream::endmsg;
        m_schemaWritten = false;
        for( int col_id = 0; col_id < m_description.numberOfTokenColumns(); col_id++ ) {
             std::string columnName = m_description.tokenColumn(col_id).name();
             addTreeBranch( columnName, CollectionBaseNames::tokenTypeName );
        }
        for( int col_id = 0; col_id < m_description.numberOfAttributeColumns(); col_id++ ) {
             const ICollectionColumn& column = m_description.attributeColumn(col_id);
             addTreeBranch( column.name(), column.type() );
        }
      }

      m_poolOut << coral::Info <<  "Root collection opened, size = " << m_tree->GetEntries() << corENDL;

      if( m_session && m_mode == ICollection::UPDATE ) {
        m_tree->SetDirectory(0);

	int n(0);
	if (!m_fileMgr) {
	  m_file->Close();
	} else {
	  n = m_fileMgr->close(m_file,"RootCollection");
	}

        if (n == 0) delete m_file; 
	m_file =0;
      }
      m_open = true;
    }
    catch( std::exception &e ) {
       m_poolOut << coral::Debug << "Open() failed with expception: " << e.what() << corENDL;
       cleanup();
       throw;
    }

     
    bool RootCollection::isOpen() const{
      return m_open;
    }

     
    ICollection::OpenMode RootCollection::openMode() const
    {
      return m_mode;
    }

     
    bool RootCollection::fileCatalogRequired() const
    {
      return m_name.find("PFN:")==0 || 
        m_name.find("FID:")==0 || 
        m_name.find("LFN:")==0; 
    }

     
    string RootCollection::retrievePFN() const {
      if (m_name.substr (0, 4) != "PFN:")
        throw pool::Exception( "In CREATE mode a PFN has to be provided",
        "RootCollection::open", 
        "RootCollection");
      return m_name.substr(4,string::npos);
    }

     
    string  RootCollection::retrieveFID() {

      FileCatalog::FileID fid="";
      string fileType="";        

      if (m_name.substr (0, 4) == "PFN:") {
        string pfn = m_name.substr(4,string::npos);
        m_fileCatalog->start();
        m_fileCatalog->lookupFileByPFN(pfn,fid,fileType);
        m_fileCatalog->commit();
      }
      else if (m_name.substr (0, 4) == "LFN:") {
        string lfn = m_name.substr(4,string::npos);
        m_fileCatalog->start();
        m_fileCatalog->lookupFileByLFN(lfn,fid);
        m_fileCatalog->commit();
      }
      else if (m_name.substr (0, 4) == "FID:") {
        fid = m_name.substr(4,string::npos);
      }else
        throw pool::Exception( "A FID, PFN or and LFN has to be provided",
        "RootCollection::retrieveFID", 
        "RootCollection");
      return fid;
    }


   string RootCollection::retrieveUniquePFN(const FileCatalog::FileID& fid)
   {
      IFileCatalog::Files       pfns;
      m_fileCatalog->start();
      m_fileCatalog->getPFNs(fid, pfns);
      m_fileCatalog->commit();
      if( pfns.empty() )
         throw pool::Exception( "This exception should never have been thrown, please send a bug report",
                                "RootCollection::retrieveUniquePFN", 
                                "RootCollection"); 
      if( pfns.size() > 1 )
         throw pool::Exception( "Cannot UPDATE or CREATE_AND_OVERWRITE since there are replicas",
                                "RootCollection::retrieveUniquePFN", 
                                "RootCollection");
      return pfns[0].first;
    }

   
    const ICollectionDescription& RootCollection::description() const
    {
      return m_description;
    }

     
    ICollectionQuery* RootCollection::newQuery()
    {
       if( !isOpen() ) {
          throw pool::Exception( "Attempt to query a closed collection.", "RootCollection::newQuery", "RootCollection" );
       }
       return new RootCollectionQuery( m_description, m_tree );
    }

  }
}
