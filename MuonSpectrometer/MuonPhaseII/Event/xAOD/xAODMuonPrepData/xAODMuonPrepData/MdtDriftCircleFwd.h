/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_MDTDRIFTCIRCLEFWD_H
#define XAODMUONPREPDATA_MDTDRIFTCIRCLEFWD_H

/** @brief Forward declaration of the xAOD::MdtDriftCircle */
namespace xAOD{
   class MdtDriftCircle_v1;
   using MdtDriftCircle = MdtDriftCircle_v1;

   class MdtDriftCircleAuxContainer_v1;
   using MdtDriftCircleAuxContainer = MdtDriftCircleAuxContainer_v1;
}
#endif
